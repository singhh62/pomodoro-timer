import React from 'react';
import {View, StyleSheet, Button} from 'react-native';
import Time from './utils/Time';
import TimeForm from './utils/timeForm'
import {mins_to_seconds, seconds_to_time} from './utils/times'


export default class App extends React.Component {
  
  state = {
    paused: true,
    isWorkTime: true,
    workTime: {startMinutes: 0, startSeconds: 0, currentMinutes: 0, currentSeconds: 0},
    breakTime:{startMinutes: 0, startSeconds: 0, currentMinutes: 0, currentSeconds: 0},
  }

  createTimeFromInput = input_time => {
    const minutes = +input_time.minutes
    const seconds = +input_time.seconds
    const total_seconds = mins_to_seconds(minutes) + seconds
    return seconds_to_time(total_seconds)
  }

  updateWorkTime = input => {
    
    this.setState({paused: true})
    const new_time = this.createTimeFromInput(input)
    this.setState({
      workTime:{
        startMinutes:   new_time.minutes,
        startSeconds:   new_time.seconds,
        currentMinutes: new_time.minutes,
        currentSeconds: new_time.seconds
      }
    })
  }
  updateBreakTime = input => {
    
    this.setState({paused: true})
    const new_time = this.createTimeFromInput(input)
    this.setState({
      breakTime: {
        startMinutes: new_time.minutes,
        startSeconds: new_time.seconds,
        currentMinutes: new_time.minutes,
        currentSeconds: new_time.seconds
      }
    })
  }

  start = () => {
    if(this.state.paused){

      this.setState({paused: false})
      const myInterval = setInterval( () => {
        if(this.state.paused){
          clearInterval(myInterval)
        }
        else{
          this.countDownTime()
        }
      },1000) 
    }
  }

  
  pause = () => (this.setState({paused: true}))

  reset = () => {
    
    this.setState({
      paused: true,
      isWorkTime: true,
      workTime : {
        ...this.state.workTime,
        currentMinutes: this.state.workTime.startMinutes,
        currentSeconds: this.state.workTime.startSeconds,
      },
      breakTime : {
        ...this.state.breakTime,
        currentMinutes: this.state.breakTime.startMinutes,
        currentSeconds: this.state.breakTime.startSeconds,
      }

    })
  }

  countDownTime = () => {

      if(this.state.isWorkTime){// if its' worktime now
        const total_worktime_seconds = this.state.workTime.currentMinutes * 60 + this.state.workTime.currentSeconds - 1
        const new_work_time = seconds_to_time(total_worktime_seconds)
        if(total_worktime_seconds < 0){ //if seconds < 0, then switch to breaktime, else, set new worktime
          this.setState({
            isWorkTime: false,
            workTime: {
              ...this.state.workTime,
              currentMinutes: this.state.workTime.startMinutes,
              currentSeconds: this.state.workTime.startSeconds,
            }
          })
        } 
        else{//set state to countdown
          this.setState({
            workTime: {
              ...this.state.workTime,
              currentMinutes: new_work_time.minutes,
              currentSeconds: new_work_time.seconds
            }
          })
        }
      }
      else{ //if it's breaktime now 
        const total_breaktime_seconds = this.state.breakTime.currentMinutes * 60 + this.state.breakTime.currentSeconds - 1
        const new_break_time = seconds_to_time(total_breaktime_seconds)
        if(total_breaktime_seconds < 0){//if seconds < 0, then switch to worktime, else set new breaktime
          this.setState({
            isWorkTime: true,
            breakTime: {
              ...this.state.breakTime,
              currentMinutes: this.state.breakTime.startMinutes,
              currentSeconds: this.state.breakTime.startSeconds
            }
          })
        }
        else{ //set state to countdown 
          this.setState({
            breakTime: {
              ...this.state.breakTime,
              currentMinutes: new_break_time.minutes,
              currentSeconds: new_break_time.seconds
            }
          })
        }
      }
    }

  render() {
    return(
      <View style={styles.mainContainer}>
            
        {/* Time View */}
        <View style={styles.timeShown}>
          {this.state.isWorkTime ? 
            <Time type="Work Time"  minutes={this.state.workTime.currentMinutes} seconds={this.state.workTime.currentSeconds}/>
            :
            <Time type="Break Time" minutes={this.state.breakTime.currentMinutes} seconds={this.state.breakTime.currentSeconds}/>
          }
        </View>
        
        {/* Button View */}
        <View style={styles.buttons}>
          <Button title="Start" onPress={this.start} />
          <Button title="Pause" onPress={this.pause} />
          <Button title="Reset" onPress={this.reset} />
        </View>

        {/* Form View */}
        <View style={styles.timeForm}>
          <TimeForm title="Work Time" onTimeChange={this.updateWorkTime}/>
          <TimeForm title="Break Time" onTimeChange={this.updateBreakTime}/>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  timeShown : {
    flex:0.5,
    justifyContent: "center"
  },
  timeForm: {
  },
  buttons:{
    flexDirection: "row",
    justifyContent: "center"
  }
})