import React from 'react'
import {View, Text, TextInput, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'

export default class TimeForm extends React.Component {
  state = {
    minutes: "",
    seconds: "",
  }

  componentDidUpdate = (prevProps, prevState) => {
    if(this.state.minutes !== prevState.minutes || this.state.seconds !== prevState.seconds) {
      this.submitChange()
    }
  }

  handleMinsChange = minutes => {
    if(+minutes >= 0 || !minutes) {
      this.setState({minutes: minutes})
    }
  }

  handleSecsChange = seconds => {
    if(+seconds >=0 || !seconds){
      this.setState({seconds: seconds})
    }
  }

  submitChange = () => {
    this.props.onTimeChange(this.state)
  }

  render(){
    return (
      <View style={styles.wrapper}>

        {/* Form View Container */}
        <View style={styles.form}>

          {/* Title */}
          <Text style={{fontWeight: 'bold'}}>{this.props.title}</Text>

          {/* Mins View */}
          <View style={styles.mins}>
            <Text>Mins:</Text>
            <TextInput value={String(this.state.minutes)} onChangeText={this.handleMinsChange} keyboardType="decimal-pad" style={styles.textInput}/>
          </View>

          {/* Secs View*/}
          <View style={styles.secs}>
            <Text>Secs:</Text>
            <TextInput value={String(this.state.seconds)} onChangeText={this.handleSecsChange} keyboardType="decimal-pad" style={styles.textInput}/>
          </View>
          
        </View>
      </View>
    )
  }
}

TimeForm.propTypes = {
  title: PropTypes.string,
  onTimeChange: PropTypes.func
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff',    
    },
  title : {
    textAlign: 'center',
  },
  form: {
    flexDirection: 'row',
    alignItems: "center",
    backgroundColor: '#ffffff',
    justifyContent: 'space-evenly',
  },
  textInput: {
    width: 30, borderBottomWidth: 1, borderColor: 'black'
  },
  mins: {
    flexDirection: 'row'
  },
  secs: {
    flexDirection: 'row'
  }
  

})
