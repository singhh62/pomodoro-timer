import {Text,View} from 'react-native'
import React from 'react'
import PropTypes from 'prop-types'

const BreakTime = props => (
  <View>
    <Text>BREAK TIME</Text>
    <Text>{String(props.minutes).padStart(2,0)} : {String(props.seconds).padStart(2,0)}</Text>
  </View>
);

BreakTime.propTypes = {
  minutes: PropTypes.number,
  seconds: PropTypes.number,
}

export default BreakTime