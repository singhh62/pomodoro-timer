import {Text,View, StyleSheet} from 'react-native'
import React from 'react'
import PropTypes from 'prop-types'

const Time = props => { 
  const mins = String(props.minutes).padStart(2,0)
  const secs = String(props.seconds).padStart(2,0)
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={{fontSize: 70}}>{props.type}</Text>
        <Text style={{fontSize: 70}}>{mins} : {secs}</Text>
      </View>
    </View>
  );
}

Time.propTypes = {
  minutes: PropTypes.number,
  seconds: PropTypes.number,
  type: PropTypes.string
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
  }
})

export default Time