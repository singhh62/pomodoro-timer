
export const seconds_to_time = input_seconds => {
    const mins = Math.floor(input_seconds / 60)
    const secs = input_seconds % 60;
    const newTime = {minutes: mins, seconds: secs}
    return newTime
  };

  export const mins_to_seconds = minutes => {
    const fullsecs = Math.floor(minutes) * 60
    const remaining = Math.round((minutes % 1) * 60)
    
    return fullsecs + remaining
  }

  export const time_to_seconds = time =>{
    const mins_to_sec = time.minutes
    return mins_to_sec + time.seconds
  }


  export const time_minus_one = input_time => {
    const new_total_seconds = input_time.minutes * 60 + input_time.seconds - 1
    return seconds_to_time(new_total_seconds)
  }